$(window).load(function(){

	(function($){
		//============= подсоединяемся к областям разметки ============
		var s_adam = Snap('#svg_adam');
		var adam_width = $('#svg_adam').width(), adam_height = $('#svg_adam').height();
		var s = Snap('#scene');
		var scene_width = $('#scene').width(), scene_height = $('#scene').height();
			
		s.attr({
			viewBox: [0, 0, 900, 900]
		});
		
		//========================= объекты ==========================
		
		//------------------------ толпа -----------------------------
		var Crowd = function(){
			this.list = [];
                        this.index = 0;
		};
		
		Crowd.prototype.push = function(man){						
			this.list.push(man);			                        
		};
                
                Crowd.prototype.hasNext = function(){						                    
                    return this.index < this.list.length;
                    
		};
                Crowd.prototype.next = function(){						
                    var t=this, item;

                    if (!t.hasNext()){
                      return null;
                    }
                    item = t.list[t.index];                    
                    t.index += 1;    
                    
                    return item;
		};
                Crowd.prototype.rewind = function(){
                    this.index = 0;
                };
		
		//------------------------ физическое лицо --------------------
		var Face = function(){}//конструктор объекта
		
                Face.prototype.adam = s_adam.select('#head'); // добыли Адамa
                
		Face.prototype.create = function(){//создающий метод
			var t = this, xt, yt;
				
			//console.log(adam_width);				
			xt = Math.floor(Math.random()*scene_width);
			yt = Math.floor(Math.random()*scene_height);
			//console.log(s);
			t.human = t.adam.clone(); //клонировали эталонное лицо
			t.human.attr({
				transform: 'translate('+ xt + ',' + yt + ')',
			});
			
			return t;
		};
                
                Face.prototype.setAnimations = function(){
                    //t.human.select('.eye_cores').
                };
            
                Face.prototype.setStyle = function(){                    
                };				
		
		Face.prototype.show = function(){ //поместили лицо на сцену
			var t = this;
			
                        //t.human.drag();
			s.append(t.human);    //поместили на сцену			
		};
		
		//коллекция декораторов
                Face.decorators = {};
		
		//расы                
                Face.decorators.black = { //негры и пигмеи
                    setStyle: function() {
                        var t=this;
                        t.uber.setStyle();
                        //console.log(t.human);
                        t.human.select('.face').attr({
                            fill:'#000000'
                        });
                    }
                };
                
                Face.decorators.mongol = { //монголоиды
                    setStyle: function() {
                        var t=this;
                        t.uber.setStyle();
                        //console.log(t.human);
                        t.human.select('.face').attr({
                            fill:'rgb(236, 224, 123)'
                        });
                    }
                };
                
                Face.decorators.white = { //белые - по-умолчанию
                    setStyle: function() {                        
                        this.uber.setStyle();                    
                    }
                };
                
                //пол
                Face.decorators.women = { //женщины
                    setStyle: function() {
                        var t=this;
                        t.uber.setStyle();
                        //console.log(t.human);
                        
                        t.human.select('.hair_short').toggleClass('hidden'); //скрываем короткие
                        t.human.select('.hair_long').toggleClass('hidden'); // показываем длинные
                        
                        t.human.select('.moustache').addClass('hidden'); //бреем усы
                        t.human.select('.beard').addClass('hidden'); //бреем бороду
                    }
                };
                
                Face.decorators.men = { //мужчины - по-умолчанию
                    setStyle: function() {
                        this.uber.setStyle();                        
                    }
                };
                
                //цвет волос
                Face.decorators.blonde = { //блондин(ка)
                    setStyle: function() {
                        var t=this;
                        t.uber.setStyle();
                        //console.log(t.human);
                        t.human.selectAll('.hair').attr({
                            fill:'#FCFCFC'
                        });
                        t.human.select('.moustache').attr({
                            fill:'#FCFCFC'
                        });
                        t.human.select('.beard').attr({
                            fill:'#FCFCFC'
                        });                        
                    }
                };
                
                Face.decorators.brune = { //брюнет(ка) - по-умолчанию
                    setStyle: function() {
                        this.uber.setStyle();                                              
                    }
                };
                
                //волосатость
                Face.decorators.noBeard = { //безбородые
                    setStyle: function() {
                        var t=this;
                        t.uber.setStyle();
                                                
                        t.human.select('.beard').addClass('hidden'); //бреем бороду
                    }
                };
                
                Face.decorators.noMoustache = { //безусые
                    setStyle: function() {
                        var t=this;
                        t.uber.setStyle();
                       
                        t.human.select('.moustache').addClass('hidden'); //бреем усы                        
                    }
                };
                
                Face.decorators.noHair = { //лысые
                    setStyle: function() {
                        var t=this;
                        t.uber.setStyle();
                        
                        t.human.select('.hair_short').addClass('hidden'); //скрываем короткие                        
                        t.human.select('.hair_long').addClass('hidden'); //скрываем длинные
                    }
                };
                
                Face.decorators.allHair = { //цельноволосатые - по-умолчанию
                    setStyle: function() {
                        this.uber.setStyle();                                                                     
                    }
                };
                
                //метод - декоратор
                Face.prototype.decorate = function(decorator){
                    var F = function(){},//временный конструктор
                        overrides = this.constructor.decorators[decorator],
                        i, newobj;

                    F.prototype = this;
                    newobj = new F();
                    newobj.uber = F.prototype; //ссылка детям на отца

                    //декорируем – переопределяем поля
                     for (i in overrides){
                        if (overrides.hasOwnProperty(i)){
                            newobj[i] = overrides[i];
                            //console.log(newobj[i]);
                      }
                    }
                    return newobj;
    
                };
                
		
		//=============== генерируем лицо по клику на кнопку =============
		
		crowd = new Crowd();
		$('.gen_it').click(function(){
			var man = new Face(); //создадим лицо			
			man.create();
                        
                        var decs = get_ext_decorators('.details .box');//выбрали декораторы из формы
                        //декорируем всем, что нашли
                        for (i in decs){
                            man = man.decorate(decs[i]);
                        }                       
                        //man = man.decorate('women').decorate('blonde');
                        man.setStyle();
			man.show();
			
			crowd.push(man); //добавим к толпе
                        
                        $('#cnt').html(Number($('#cnt').html())+1);//видный счетчик для удобства                        
		});
                
                //выбираем декораторы из формы
                var get_ext_decorators = function(selector){
                    var dec_arr = [];
                    $(selector).each(function(){
                        var deco = $(this).children('input[checked="checked"]').val();
                        dec_arr.push(deco);
                        //console.log(deco);
                    });
                    return dec_arr;
                };
                
                //=================  бреем бороды ВСЕМ  =================
                $('.rm_boroda').click(function(){
                    crowd.rewind();
                    while(crowd.hasNext()){
                        crowd.next().human.select('.beard').addClass('hidden'); //бреем усы                          
                    }    
                });
		
                
               //==================== обеспечительные меры ====================
               
                //оживляем радио-кнопки: чтобы checked реально ставился по клику мыши
                $('input[type="radio"]').click(function(){
                   var id = $(this).attr('id');
                   $(this).parent().children('input[type="radio"]').each(function(){
                       if ($(this).attr('id') === id){
                           $(this).attr('checked',"checked");
                       } else {
                           $(this).removeAttr('checked');
                       }                       
                   });
                    
                });
	   		
	}(jQuery))    
});
